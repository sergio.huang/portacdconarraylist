import java.util.ArrayList;
import java.util.Objects;
public class PortaCD {
    private ArrayList<CD>dischi;

    public PortaCD() {
        dischi=new ArrayList<CD>();
    }

    public CD getDisco(int i) {
        if(i<=dischi.size()){
            return dischi.get(i);
        }
        return null;
    }

    public void addDischi(CD cd) {
        if(!dischi.contains(cd)){
            dischi.add(cd);
        }
    }

    public void getN(int i, CD cd) {
        dischi.set(i, cd);
    }

    public int getSize() {
        return dischi.size();
    }

    public void killCD(CD cd){
        for (int i = 0; i < getSize(); i++) {
            if(dischi.get(i).equals(cd)){
                dischi.remove(i);
            }
        }
    }

    public CD cercaCDperTitolo(String titolo){
        for (int i = 0; i < dischi.size(); i++) {
            if(dischi.get(i).getTitolo()!=null&&dischi.get(i).getTitolo().equals(titolo)){
                return dischi.get(i);
            }
        }
        return null;
    }

    public boolean confrontaCollezione(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof PortaCD)) {
            return false;
        }
        PortaCD portaCD = (PortaCD) o;
        return Objects.equals(dischi, portaCD.dischi);
    }
}