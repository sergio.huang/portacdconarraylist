public class CD {
    private String titolo, autore;
    private int durata, brani;

    public CD() {
    }

    public CD(String titolo, String autore, int durata, int brani) {
        this.titolo = titolo;
        this.autore = autore;
        this.durata = durata;
        this.brani = brani;
    }

    public String getTitolo() {
        return this.titolo;
    }

    public void setTitolo(String titolo) {
        this.titolo = titolo;
    }

    public String getAutore() {
        return this.autore;
    }

    public void setAutore(String autore) {
        this.autore = autore;
    }

    public int getDurata() {
        return this.durata;
    }

    public void setDurata(int durata) {
        this.durata = durata;
    }

    public int getBrani() {
        return this.brani;
    }

    public void setBrani(int brani) {
        this.brani = brani;
    }

    public CD titolo(String titolo) {
        setTitolo(titolo);
        return this;
    }

    public CD autore(String autore) {
        setAutore(autore);
        return this;
    }

    public CD durata(int durata) {
        setDurata(durata);
        return this;
    }

    public CD brani(int brani) {
        setBrani(brani);
        return this;
    }
    @Override
    public String toString() {
        return "{" +
            " titolo='" + getTitolo() + "'" +
            ", autore='" + getAutore() + "'" +
            ", durata='" + getDurata() + "'" +
            ", brani='" + getBrani() + "'" +
            "}";
    }
    public int comparaDurata(CD cd2){
        if(this.durata==cd2.durata){
            return 0;
        }else if(this.durata<cd2.durata){
            return -1;
        }
        return 1;
    }

}
